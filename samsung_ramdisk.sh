#!/bin/sh

# update ramdisk from local boot.img
# by arter97
#
# dependencies : p7zip, unpackbootimg, cpio

rm -rf tmp
mkdir tmp
cd tmp

unpackbootimg -i ../boot.img

cd ..
rm -rf samsung
mkdir samsung
cd samsung
gunzip -c ../tmp/boot.img-ramdisk.gz | cpio -i

cd ..
rm -f boot.img
rm -rf tmp
git reset

echo ""
echo DONE!
