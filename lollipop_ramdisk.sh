#!/bin/sh

# update ramdisk from http://get.cm/?device=i9300 latest nightly
# by arter97
#
# dependencies : p7zip, unpackbootimg, cpio

rm -rf tmp
mkdir tmp
cd tmp

7z x "$@" boot.img
unpackbootimg -i boot.img

cd ..
rm -rf lollipop
mkdir lollipop
cd lollipop
gunzip -c ../tmp/boot.img-ramdisk.gz | cpio -i

cd ..
rm -rf tmp
git reset

echo ""
echo DONE!
