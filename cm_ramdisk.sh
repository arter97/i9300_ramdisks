#!/bin/sh

# update ramdisk from http://get.cm/?device=i9300 latest nightly
# by arter97
#
# dependencies : p7zip, unpackbootimg, cpio

rm -rf tmp
mkdir tmp
cd tmp
export date=$(($(date +'%Y%m%d') + 2))
wget http://get.cm/?device=i9300 || exit 1
until cat index.html\?device\=i9300 | grep $date | grep cm-10.2 | tr '\"' '\n' | grep jenkins | grep -v http ; do
	export date=$(($date - 1))
done
rm -rf /tmp/arter97_update_ramdisk
mkdir /tmp/arter97_update_ramdisk

until wget -P /tmp/arter97_update_ramdisk/ http://get.cm$(cat index.html\?device\=i9300 | grep $date | tr '\"' '\n' | grep jenkins | grep -v http); do
	rm -rf /tmp/arter97_update_ramdisk/*.zip
done

7z x /tmp/arter97_update_ramdisk/*.zip boot.img
unpackbootimg -i boot.img

cd ..
rm -rf cm
mkdir cm
cd cm
gunzip -c ../tmp/boot.img-ramdisk.gz | cpio -i

cd ..
rm -rf tmp
rm -rf /tmp/arter97_update_ramdisk
git reset

echo ""
echo DONE!
